package main

import (
	"fmt"
	"net/http"
	"os"
)

var (
	count = 0
)

func hello(w http.ResponseWriter, req *http.Request) {
	count = count + 1
	fmt.Fprintf(w, "Hello World!\n")
}

func status(w http.ResponseWriter, req *http.Request) {
	count = count + 1
	fmt.Fprintf(w, "Hello from %s and here is the count: %d\n", os.Getenv("HOSTNAME"), count)
}

func main() {

	http.HandleFunc("/", hello)
	http.HandleFunc("/status", status)

	port := os.Getenv("PORT")

	if port == "" {
		port = "8080"
	}

	http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
}
